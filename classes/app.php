<?php
/**
 * The application object presented to the user
 *
 * The application (i.e. app) is the main object the user has to do with
 * It reprensents an application for the TreasureBox and an abstraction
 * layer with the Docker containers and images.
 *
 */

class app {
	function __construct($di,$appType){
		$this->di = $di;
		$this->appType = $appType;
	}
	static function listActive($di) {
		$containers = static::filterContainers($di,dockerContainer::listActive($di));
		foreach ($containers as $k=>$v){
			if (substr($v->Status,0,2)!='Up')
				unset($containers[$k]);
		}
		return $containers;
	}

	static function listAvailable($di) {
		$containedApps=array();
		$containers = static::filterContainers($di,dockerContainer::listAll($di));
		foreach ($containers as $k=>$v){
			$containedApps[]=$containers[$k]->getAppName();
			if (substr($v->Status,0,2)=='Up')
				unset($containers[$k]);
		}
		$images = static::filterImages($di,dockerImage::listAvailable($di));
		foreach ($images as $k=>$v){
			if (in_array($v->getAppName(),$containedApps))
				unset($images[$k]);
		}
		return array_merge($containers,$images);
	}

	static function listInstallable($di){
		$appConfigFiles = appConfigFile::listUsable($di);
		$appConfigFiles = static::filterAppConfigFiles($di,$appConfigFiles);
		return $appConfigFiles;
	}

	static function filterAppConfigFiles($di,$listFiles){
		foreach ($listFiles as $k=>$v){
			if (!$v['viewInStore'] || !array_key_exists($di->services['architecture'],$v['dockerImageNamePrefix']))
				unset($listFiles[$k]);
		}
		return array_map('static::convertArray2object',array_fill(0,count($listFiles),$di),array_fill(0,count($listFiles),'configFile'), $listFiles);
	}

	static function filterContainers($di,$listContainers){
		foreach ($listContainers as $k => $v){
			if (substr($v['Image'],0,strlen($di->services['pirateKey4images']))!=$di->services['pirateKey4images']){
				unset($listContainers[$k]);
			}
		}
		return array_map('static::convertArray2object',array_fill(0,count($listContainers),$di),array_fill(0,count($listContainers),'container'),$listContainers);
	}

	static function filterImages($di,$listImages){
		foreach ($listImages as $k => $v){
			if (substr($v['RepoTags'][0],0,strlen($di->services['pirateKey4images']))!=$di->services['pirateKey4images']){
				unset($listImages[$k]);
			}
		}
		return array_map('static::convertArray2object',array_fill(0,count($listImages),$di),array_fill(0,count($listImages),'image'),$listImages);
	}

	function getAppName() {
		if (isset($this->AppName)) return $this->AppName;
		if (isset($this->Image))
			$name = $this->Image;
		elseif (isset($this->RepoTags[0]))
			$name = $this->RepoTags[0];
		$length = strlen($name);
		$start = strlen($this->di->services['pirateKey4images']);
		$end = $length - strpos($name,':');
		$this->AppName = ucfirst(substr($name,$start,-$end));
		return $this->AppName;
	}

	function getDescription(){
		return $this->description?:"Pas de description trouvée !";
	}

	function getHelpUrl(){
		return '#';
	}

	static function convertArray2object($di,$type,$arr) {
		$class = get_called_class();
		$obj = new $class($di,$type);
		foreach ($arr as $k => $v){
			$obj->$k = $v;
		}
		$obj->appType = $type;
		if (!isset($obj->description)) {
			$config = appConfigFile::parseFileContentFromAppName($di,$obj->getAppName());
			foreach ($config as $k => $v){
				$obj->$k = $v;
			}
		}
		return $obj;
	}

}
