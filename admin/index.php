<?php
include_once('../config/config.php');
if (array_key_exists('action',$_GET)){
	switch ($_GET['action']) {
		case 'stop':
			dockerContainer::stop($di,$_GET['id']);
			break;
		case 'start':
			switch ($_GET['type']) {
				case 'container':
					dockerContainer::start($di,$_GET['id']);
					break 2;
				case 'image':
					dockerImage::start($di,$_GET['id']);
					break 2;
			}
		case 'install':
			var_dump(appConfigFile::install($di,$_GET['name']));
			break;
	}

}
?>
<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>TreasureBox - Le Coffre au Trésor !</title>
	<link href="../lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
	<link rel="manifest" href="/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-md-12 text-center">
				<div class="page-header clearfix">
					<div class="col-md-3 pull-right">
						<div class="panel panel-primary">
							<div class="panel-heading">Administrateur</div>
								<div class="panel-body">
								<ul class="list-inline">
									<li><a href="config.html" class="btn btn-danger active" role="button">Administration</a></li>
									<li><a href="../" class="btn btn-primary active" role="button">Sortir</a></li>
								</ul>
							</div>
						</div>
					</div>
					<img src="../img/seeraiwer-bandeau.png">
				</div>
					<h1>Applications</h1>
					<div class="panel panel-primary table-responsive">
						<div class="panel-heading">Applications actives</div>
						<div class="panel-body">
							<p>Ci-dessous, vous trouvez la liste des applications actuellement actives sur votre TreasureBox</p>
						</div>
						<table class="table table-striped table-hover text-left">
							<thead><tr><th style="max-width:10em;">Actions</th><th>Nom</th><th>Description</th></tr></thead>
							<tbody>
<?php
$activeApps = app::listActive($di);
$apps = array();
foreach ($activeApps as $activeApp){
	$apps[] = $activeApp->getAppName();
	print '<tr><td><a title="Accéder" target="_blank" href="//';
	print $_SERVER['HTTP_HOST'];
	print '/';
	print $activeApp->getAppName();
	print '"><span class="glyphicon glyphicon-send" aria-hidden="true"></span></a> ';
	print '<a href="';
	print $activeApp->getHelpUrl();
	print '" title="Aide"><span class="glyphicon glyphicon-question-sign" aria-hidden="true"></span></a> <a class="pull-right" href="?action=stop&id=';
	print $activeApp->Id;
	print '" title="Arrêter"><span class="glyphicon glyphicon-off" aria-hidden="true"></span></a></td><td>';
	print $activeApp->getAppName();
	print '</td><td>';
	print $activeApp->getDescription();
	print '</td></tr>';
}
?>
							</tbody>
						</table>
					</div>

					<div class="panel panel-primary table-responsive">
						<div class="panel-heading">Applications inactives</div>
						<div class="panel-body">
							<p>Ci-dessous, vous trouvez la liste des applications actuellement installées mais inactives sur votre TreasureBox</p>
						</div>
						<table class="table table-striped table-hover text-left">
							<thead><tr><th style="max-width:10em;">Actions</th><th>Nom</th><th>Description</th></tr></thead>
							<tbody>
<?php
$availableApps = app::listAvailable($di);
foreach ($availableApps as $availableApp){
	$apps[] = $availableApp->getAppName();
	print '<tr><td><a href="?action=start&id=';
	print $availableApp->Id.'&type='.$availableApp->appType;
	print '" title="Lancer"><span class="glyphicon glyphicon-play" aria-hidden="true"></span></a> <a href="?action=parameters&id=';
	print $availableApp->Id;
	print '" title="Paramétrer"><span class="glyphicon glyphicon-cog" aria-hidden="true"></span></a> <a href="';
	print $availableApp->getHelpUrl();
	print '" title="Aide"><span class="glyphicon glyphicon-question-sign" aria-hidden="true"></span></a> <a class="pull-right" href="?action=delete&id=';
	print $availableApp->Id;
	print '" title="Supprimer"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a></td><td>';
	print $availableApp->getAppName();
	print '</td><td>';
	print $availableApp->getDescription();
	print '</td></tr>';
}
?>
							</tbody>
						</table>
					</div>

					<div class="panel panel-primary table-responsive">
						<div class="panel-heading">Applications installables</div>
						<div class="panel-body">
							<p>Ci-dessous, vous trouvez la liste des applications pouvant être installées sur votre TreasureBox</p>
							<a href="#" class="btn btn-primary btn-lg active" role="button">Actualiser la liste</a>
							<a href="#" class="btn btn-success btn-lg active" role="button">Visiter l'Île aux Trésors</a>
						</div>
						<table class="table table-striped table-hover text-left">
							<thead><tr><th style="max-width:10em;">Actions</th><th>Nom</th><th>Description</th></tr></thead>
							<tbody>
<?php
$installableApps = app::listInstallable($di);
foreach ($installableApps as $installableApp){
	if (in_array($installableApp->getAppName(),$apps)) continue;
	print '<tr><td><a title="Installer" href="?action=install&name=';
	print $installableApp->getAppName();
	print '"><span class="glyphicon glyphicon-save" aria-hidden="true"></span></a> ';
	if ($installableApp->learnMoreUrl){
		print '<a title="En savoir plus" target="_blank" href="';
		print $installableApp->learnMoreUrl;
		print '"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a>';
	}
	print '</td><td>';
	print $installableApp->getAppName();
	print '</td><td>';
	print $installableApp->getDescription();
	print '</td>';
	print '</tr>';
}
?>
							</tbody>
						</table>
					</div>

				</div>
			</div>
		</div>

	<script src="../lib/jquery.min.js"></script>
	<script src="../lib/bootstrap/js/bootstrap.min.js"></script>
</body>
</html>


