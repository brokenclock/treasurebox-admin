<?php
/**
 * Handles Docker containers
 *
 * This class is dedicated to the managment of Docker containers
 *
 */

class dockerContainer {
	function __contruct($di,$array){
		foreach ($array as $k=>$v){
			$this->$k = $v;
		}
		$this->di = $di;
	}

	/** list active containers */
	static function listActive($di){
		return $di->services['dockerRequester']->getContainers();
	}

	/** list all containers */
	static function listAll($di){
		return $di->services['dockerRequester']->getAllContainers();
	}

	static function stop($di,$id){
		return $di->services['dockerRequester']->stop($id);
	}

	static function start($di,$id){
		return $di->services['dockerRequester']->start($id);
	}
}
