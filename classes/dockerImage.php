<?php
/**
 * Handles Docker imagess
 *
 * This class is dedicated to the managment of Docker images
 *
 */

class dockerImage {
	function __contruct($di,$array){
		foreach ($array as $k=>$v){
			$this->$k = $v;
		}
		$this->di = $di;
	}

	/** list active containers */
	static function listAvailable($di){
		return $di->services['dockerRequester']->getImages();
	}

	static function start($di,$id){
		return $di->services['dockerRequester']->startFromImage($id);
	}

}

