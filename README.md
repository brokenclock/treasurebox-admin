# TreasureBox Administration Web Interface

## Descriptin
Ceci est un sous-projet pour le projet TreasureBox.

Il consiste à créer une interface d'administration permettant d'ajouter, supprimer, accéder... des applications pour la TreasureBox.

## Plate-forme cible

La TreasureBox cible le [Raspberry Pi](https://www.raspberrypi.org/) comme environnement de production.

## Licence
Ce projet est distribué sous licence [AGPLv3](https://gnu.org/licenses/agpl.html).