<?php
include_once(__DIR__.'/../classes/autoload.php');
$di = new stdClass();
$di->services['dockerRequester']= new dockerRequester($di,new dockerConnexion($di));
$di->services['baseUrl'] = 'http://localhost/projects/TreasureBox/treasurebox-admin/';
$di->services['imagesStoreDirectory'] = realpath(__DIR__.'/../data/store/');
$di->services['architecture'] = substr(shell_exec('lscpu|grep Architecture|awk \'NF>1{print $NF}\''),0,-1);
$di->services['pirateKey4images'] = 'taophp/tbx-'.substr($di->services['architecture'],0,3).'-';
