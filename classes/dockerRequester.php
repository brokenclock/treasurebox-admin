<?php
/**
 * Request the Docker daemon
 *
 * Send request to the docker daemon the right way
 *
 */

class dockerRequester {
	protected $dockerConnexion;
	protected $di;
	protected $getCmd = array(
		'ping'					=> '/_ping',
		'containers'		=> '/containers/json',
		'allcontainers'	=> '/containers/json?all=1',
		'info'					=> '/info',
		'version'				=> '/version',
		'images'				=> '/images/json',
		'allimages'			=> '/images/json?all=1',
	);

	function __construct($di,dockerConnexion $dockerConnexion) {
		$this->dockerConnexion = $dockerConnexion;
		$this->di = $di;
	}

	function __call($method,$args) {
		if (substr($method,0,3)=='get') {
			$cmd = strtolower(substr($method,3));
			if (array_key_exists($cmd,$this->getCmd)) {
				$request = static::finalizeRequest('GET '.$this->getCmd[$cmd]);
				return $this->dockerConnexion->writeAndGetResponse($request);
			}
		}
	}

	static function finalizeRequest($request,$contentType=false,$content=null){
		$toAdd = array(
			$request.' HTTP/1.1',
			'Host: impossible-exil.info',
			'User-agent: PHP',
		);
		if ($contentType) $toAdd[] = 'Content-Type: '.$contentType;
		if ($content) $toAdd[] = 'Content-length: '.(strlen($content));
		$toAdd[]= 'Connection: Close';
		if ($content) $toAdd[]="\r\n".$content;
		$toAdd[]= "\r\n";
		return join("\r\n", $toAdd);
	}

	function ping(){
		return $this->getPing();
	}

	function stop($id){
		$request = static::finalizeRequest('POST /containers/'.$id.'/stop?t=5');
		return $this->dockerConnexion->writeAndGetResponse($request);
	}

	function start($id){
		$request = static::finalizeRequest('POST /containers/'.$id.'/start');
		return $this->dockerConnexion->writeAndGetResponse($request);
	}

	function startFromImage($id){
		/** @TODO: https://docs.docker.com/reference/api/docker_remote_api_v1.20/#create-a-container */
		$o = new stdClass();
		$json = json_encode(array(
			'Hostname'=>'',
			'User'=>'',
			'Memory'=>0,
			'MemorySwap'=>0,
			'AttachStdin'=>false,
			'AttachStdout'=>true,
			'AttachStderr'=>true,
			'PortSpecs'=>null,
			'Privileged'=>false,
			'Tty'=>false,
			'OpenStdin'=>false,
			'StdinOnce'=>false,
			'Env'=>null,
			'Dns'=>null,
			'Image'=>$id,
			'Volumes'=>$o,
			'VolumesFrom'=>'',
			'WorkingDir'=>'',
		));
		$request = static::finalizeRequest('POST /containers/create','application/json',$json);
		$t = $this->dockerConnexion->writeAndGetResponse($request);
		return $t;
	}

	function install($app){
		$request ='POST /build HTTP/1.1'."\r\n";
		$request ='X-Registry-Config:'."\r\n";
		$request.='Content-type: application/tar'."\r\n";
		$request.='Connection: Close'."\r\n\r\n";
		//$request.= '{{ ';
		$request.= $app->tarContent;
		//$request.= ' }}';
		var_dump($request);
		return $this->dockerConnexion->writeAndGetResponse($request);
	}

}
