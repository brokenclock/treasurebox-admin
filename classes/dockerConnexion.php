<?php
/**
 * Connect PHP to the Docker daemon
 *
 * Etablish a connexion to de Docker Daemon.
 *
 * With the default configuration, the web server user (i.e. www-data
 * on Debian) needs to be member of the group docker. To do so, exectute
 * the following command (example for Debian):
 *
 * 	`sudo addgroup www-data docker`
 *
 */

class dockerConnexion {
	protected $socket;
	protected $domain;
	protected $type;
	protected $protocol;
	protected $address;
	protected $port;

	function __destruct(){
		socket_close($this->socket);
	}

	function __construct($di,$domain = AF_UNIX, $type = SOCK_STREAM, $protocol = 0, $address = "/var/run/docker.sock", $port = null) {
		$this->domain = $domain;
		$this->type = $type;
		$this->protocol = $protocol;
		$this->address = $address;
		$this->port = $port;
	}

	protected function connect(){
		if (!$this->socket = socket_create($this->domain, $this->type, $this->protocol)) {
			$err=socket_last_error($this->socket);
			throw new Exception('Impossible to create the socket. Error '.$err.'. '.socket_strerror($err));
		}
		if (!$this->connexion = socket_connect($this->socket,$this->address,$this->port)) {
			$err=socket_last_error($this->socket);
			throw new Exception('Impossible to connect the socket. Error '.$err.'. '.socket_strerror($err));
		}
		return true;
	}

	function writeAndGetResponse($request){
		$this->connect();
		$result = false;
		socket_write($this->socket,$request);
		while ($out = socket_read($this->socket, 2048)) {
			$result.=$out;
		}
		list($headers,$body)=explode("\n\r",$result);
		list($statusString,$contentType)=explode("\n",$headers);
		$statusString=substr($statusString,0,-1);
		if (substr($contentType,0,-1)=='Content-Type: application/json')
			$body = json_decode($body,true);
		return $body;
	}
}
