<?php
/**
 * Handles configuration files of applications as Docker images
 *
 */

class appConfigFile {
	public $di;

	function __construct($di,$input){
		if (is_string($input)){
			$array = static::parseFileContentFromAppName($di,$input);
		} else if (is_array($input)) {
			$array = $input;
		}
		foreach ($array as $k=>$v){
			$this->$k = $v;
		}
		$this->di = $di;
	}

	static function install($di,$name){
		$class = get_called_class();
		$app = new $class($di,$name);
		return $app->installMe();
	}

	function installMe(){
		$tarFileName = sys_get_temp_dir().'/'.'tbx.tar';
		$tarFile = new PharData($tarFileName,null,null,Phar::TAR);
		$content = "FROM ".$this->dockerFrom[$this->di->services['architecture']]."\n";
		$content.= $this->dockerFile;
		$tarFile->addFromString('Dockerfile',$content);
		$tarFile->compress(Phar::GZ);
		$this->tarContent = file_get_contents($tarFileName.'.gz');
		unlink($tarFileName.'.gz');
		unlink($tarFileName);
		return $this->di->services['dockerRequester']->install($this);
	}

	static function listUsable($di) {
		$usables = array();
		$dir = dir($di->services['imagesStoreDirectory']);
		while ($file = $dir->read()){
			if ($file!='.' && $file!='..') {
				$usables[] = static::parseFileContentFromAppName($di,$file);
			}
		}
		return $usables;
	}

	static function parseFileContentFromAppName($di,$appName){
		$content = file_get_contents($di->services['imagesStoreDirectory'].'/'.strtolower($appName));
		return json_decode($content,true);
	}
}
